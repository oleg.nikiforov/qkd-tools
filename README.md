# qkd-tools

**Overview**

This is the control software for the quantum key distribution experiment, developed and used at the [Laser and Quantum Optics group (LQO)](http://www.iap.tu-darmstadt.de/lqo/research/qiv/) within the Technische Universität Darmstadt, Germany. The experiment is developed by Sabine Euler [1] and several undergraduate students [2-6].

The software structure is described in detail in [3] and [5]. The software implementation for both parties, Alice and Bob, is nearly identical and consists of following parts:
    

- Data Acquisition, which is performed by an FPGA, based on an adapted open source solution, provided by S. Polyakov, NIST [2,7]. The FPGA board is connected per USB to a PC. Key sifting is carried out by the FPGA using an additional classical channel.
- Error Correction, which is based on the slightly modified version of a library *ldpclib*, implemented by Radford Neal [8]. It uses Low-density parity-check matrices. The modification refer to the fact that the parity bits transmitted over the classical channel are error free, s.t. the error probability for such bits is always zero.
- Privacy amplification, which is carried out by application of two-universal hash function onto the error corrected key, i.e. by its multiplication with a Toeplitz Matrix [5].   
- Classical communication, that uses TCP for data exchange. 

Feel free to contact us in case of questions.

**References**

[1] S. Euler. [Preparation and Characterization of Single Photons from PDC in PPKTP for Applications in Quantum Information](https://tuprints.ulb.tu-darmstadt.de/7183/19/Dissertation_Euler.pdf), PhD thesis, 2017.

[2] T. Diehl. Quantum cryptography - Data Acquisition in Bob‘s Module. Bachelor‘s thesis. Technische Universität Darmstadt, 2009.

[3] T. Diehl. Quantum cryptography – Development of a Line for Quantum Key Distribution. Master‘s thesis. Technische Universität Darmstadt, 2011.

[4] M. Ober. Error-correcting Codes with Low Density Matrices for Quantum Cryptography. Bachelor‘s thesis. Technische Universität Darmstadt, 2011.

[5] P. Notz. Implementing a Post-Processing Procedure for Quantum Cryptography. Bachelor‘s thesis. Technische Universität Darmstadt, 2012.

[6] S. Lehmann. Test of a Line for Quantum Key Distribution. Master‘s thesis. Technische Universität Darmstadt, 2014.

[7] S: Polyakov: [Simple and Inexpensive FPGA-based Fast Time Resolving Acquisition Board](http://www.nist.gov/pml/div684/grp03/multicoincidence.cfm), Januar 2010. – Technical Description, Installation and Operation Manual.

[8] R. Neal: [Software for Low Density Parity Check Codes](http://www.cs.utoronto.ca/~radford/ftp/). LDPC-2012-02-11. Version: Februar 2012.



**Copyright**

Control software for the quantum key distribution experiment, developed and used in the Laser and Quantum Optics group (LQO) within the Technische Universität Darmstadt, Germany. 
Copyright (c) 2012 LQO, TU Darmstadt.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.Open source software for control of the quantum key distribution and its postprocessing.


**Keywords**
Quantum Key Distribution, Post-processing, Error Correction, Low-Density Parity-Check Code, Privacy Amplification 

**Contact**

oleg.nikiforov@physik.tu-darmstadt.de
