/* QkdMgr.cpp - manages all steps needed from FPGA to final key (text file) */

/*
Control software for the quantum key distribution experiment, developed and used in the Laser and Quantum Optics group (LQO) within the Technische Universität Darmstadt, Germany. 
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (c) 2012 by Pascal Notz
 */

#include "QkdMgr.h"

qkdtools::QkdMgr::QkdMgr()
{
	myDevice=new qkdtools::DevMgr();
	myThreadMgr=new qkdtools::ThreadMgr();
	myPrintOut=NULL;

	myCurConfig.autoMode=false;

	for(int i=0;i<6;i++)
	{
		hasParam[i]=false;
	}
	initDone=false;

	//create queues
	skQueue=new qkdtools::KeyQueue();
	finalKeyQueue=new qkdtools::KeyQueue();
}

qkdtools::QkdMgr::~QkdMgr()
{
	if(myPrintOut!=NULL)
	{
		delete myPrintOut;
	}

	delete myDevice;
	delete myThreadMgr;
	delete skQueue;
	delete finalKeyQueue;
}

//adds a date and time stamp to given filename
//d:\finalout.txt -> d:\finalout_20120329_1623.txt
char* addDateTimeToFilename(char* filename)
{
	SYSTEMTIME st;
    GetSystemTime(&st);
    
	//find last '.' in filename
	char *lastDot;
	lastDot=strrchr(filename,'.');
	int lastOcc=lastDot-filename+1;

	char* newFilename=new char[strlen(filename) + 16];
	strncpy(newFilename, filename, lastOcc-1);

	char tmpStr[20];
	sprintf(tmpStr, "_%.4d%.2d%.2d_%.2d%.2d" ,st.wYear,st.wMonth,st.wDay,st.wHour,st.wMinute);
	strcpy(newFilename + lastOcc - 1, tmpStr);

	strcpy(tmpStr, lastDot);
	strcat(newFilename, tmpStr);

	return newFilename;
}

bool qkdtools::QkdMgr::checkReady()
{
	for(int i=0;i<6;i++)
	{
		if(!hasParam[i])
		{
			return false;
		}
	}

	return true;
}

void qkdtools::QkdMgr::setDelays(int delays[5])
{
	for(int i=0;i<5;i++)
	{
		myCurConfig.delays[i]=delays[i];
	}

	hasParam[1]=true;
}

void qkdtools::QkdMgr::setActAsAlice(bool actAsAlice)
{
	myCurConfig.actAsAlice=actAsAlice;

	hasParam[0]=true;
}

void qkdtools::QkdMgr::setPrintOutFilename(char* printOutFilename)
{
	myCurConfig.printOutFilename=printOutFilename;

	hasParam[5]=true;
}

void qkdtools::QkdMgr::setNetworkData(char* connectToIP, int Port1, int Port2, int Port3)
{
	myCurConfig.connectToIP=connectToIP;
	myCurConfig.Ports[0]=Port1;
	myCurConfig.Ports[1]=Port2;
	myCurConfig.Ports[2]=Port3;

	hasParam[2]=true;
}

void qkdtools::QkdMgr::setPAKeyLength(int paKeyLen)
{
	myCurConfig.paKeyLen=paKeyLen;

	hasParam[3]=true;
}

void qkdtools::QkdMgr::setLDPCMatFiles(char* pchk_filename, char* gen_filename)
{
	myCurConfig.pchk_filename=pchk_filename;
	myCurConfig.gen_filename=gen_filename;

	hasParam[4]=true;
}

void qkdtools::QkdMgr::readConfigFromFile(char* filename)
{

	//read config file
	std::string line;
	std::ifstream qkdconfigfile (filename);
	if (qkdconfigfile.is_open())
	{
		//first: header line
		std::getline(qkdconfigfile,line);

		//automatic mode
		std::getline (qkdconfigfile,line);
		myCurConfig.autoMode=(line.substr(0,1).compare("1")==0);

		//actAsAlice
		std::getline (qkdconfigfile,line);
		myCurConfig.actAsAlice=(line.substr(0,1).compare("1")==0);
		
		for(int i=0;i<5;i++) //read delays
		{
			std::getline (qkdconfigfile,line);
			myCurConfig.delays[i]=atoi(line.c_str());
		}

		//read IP address
		std::getline(qkdconfigfile,line);
		myCurConfig.connectToIP=new char[min(line.length()+1, 16)];
		strcpy(myCurConfig.connectToIP, line.c_str());
	
		for(int i=0;i<4;i++) //read ports
		{
			std::getline (qkdconfigfile,line);
			myCurConfig.Ports[i]=atoi(line.c_str());
		}

		//read PALength
		std::getline (qkdconfigfile,line);
		myCurConfig.paKeyLen=atoi(line.c_str());

		//read LDPCMatFiles
		std::getline(qkdconfigfile,line);
		myCurConfig.pchk_filename=new char[line.length() + 1]; //add one char for \0
		strcpy(myCurConfig.pchk_filename, line.c_str());
		std::getline(qkdconfigfile,line);
		myCurConfig.gen_filename=new char[line.length() + 1]; //add one char for \0
		strcpy(myCurConfig.gen_filename, line.c_str());

		//read printOutFilename
		std::getline(qkdconfigfile,line);
		myCurConfig.printOutFilename=new char[line.length() + 1]; //add one char for \0
		strcpy(myCurConfig.printOutFilename, line.c_str());

		qkdconfigfile.close();
	}
	else
	{
		//std::cout << "file error" << std::endl;
		return;
	}


	for(int i=0;i<6;i++)
	{
		hasParam[i]=true;
	}

	initQkd();
}

void qkdtools::QkdMgr::initQkd()
{
	if(!checkReady())
	{
		return;
	}

	myDevice->setQueue(skQueue);

	//open device (fpga) connection
	/*if(!myDevice->openDev())
	{
		return;
	}*/

	myThreadMgr->setActAsAlice(myCurConfig.actAsAlice);

	myThreadMgr->setNetworkData(myCurConfig.connectToIP, myCurConfig.Ports[0], myCurConfig.Ports[1], myCurConfig.Ports[2]);

	myThreadMgr->setPAKeyLength(myCurConfig.paKeyLen);

	myThreadMgr->setLDPCMatFiles(myCurConfig.pchk_filename, myCurConfig.gen_filename);

	myThreadMgr->setKeyQueue(skQueue);

	myThreadMgr->setFinalKeyQueue(finalKeyQueue);

	if(myPrintOut!=NULL)
	{
		delete myPrintOut;
	}
	myPrintOut=new qkdtools::printOutKey(addDateTimeToFilename(myCurConfig.printOutFilename), finalKeyQueue);

	initDone=true;


	if(myCurConfig.autoMode) //Auto Mode allows controlling Bob via Network
	{
		myNetCmd=new qkdtools::NetworkMgr();
		myNetCmd->setNetworkParam(myCurConfig.connectToIP, myCurConfig.Ports[3], myCurConfig.Ports[3]);

		if(myCurConfig.actAsAlice) //open network connection to Bob
		{
			myNetCmd->connectSocket();
		}
		else
		{
			//Bob: start thread to receive net commands
			myNetCmd->bindSocket();

			unsigned threadID;
			myCmdThreadCom.keepRunning=true;
			myCmdThreadCom.netdevice=myNetCmd;
			myCmdThreadCom.myQkdMgr=this;

			//start thread
			_beginthreadex(NULL, 0, receiveCmds, &myCmdThreadCom, 0, &threadID);
	
		}

	}

}

bool qkdtools::QkdMgr::start()
{
	if(!initDone)
	{
		return false;
	}

	if(myCurConfig.autoMode && myCurConfig.actAsAlice) //Alice sends cmd to Bob
	{
		char cmd=1; //start
		int len=1;
		myNetCmd->sendData(&cmd, len);
	}

	//start device for data collection
/*	if(!myDevice->setDelayAndStart(myCurConfig.delays))
	{
		return false;
	}
	if(!myDevice->startDataCollectThread())
	{
		return false;
	}
*/
	if(!myThreadMgr->startThread(0)) //valid
	{
		return false;
	}
	if(!myThreadMgr->startThread(1)) //EC
	{
		return false;
	}
	if(!myThreadMgr->startThread(2)) //PA
	{
		return false;
	}
	if(!myPrintOut->startPrintOut()) //print out key
	{
		return false;
	}

	return true;
}

bool qkdtools::QkdMgr::stop()
{
	if(!initDone)
	{
		return false;
	}

	if(myCurConfig.autoMode && myCurConfig.actAsAlice) //Alice sends cmd to Bob
	{
		char cmd=2; //stop
		int len=1;
		myNetCmd->sendData(&cmd, len);
	}

	//stop device for data collection
/*	if(!myDevice->stopDataCollectThread())
	{
		return false;
	}
	if(!myDevice->stop())
	{
		return false;
	}
*/
	if(!myThreadMgr->stopThread(0)) //valid
	{
		return false;
	}
	if(!myThreadMgr->stopThread(1)) //EC
	{
		return false;
	}
	if(!myThreadMgr->stopThread(2)) //PA
	{
		return false;
	}
	myPrintOut->stopPrintOut(); //print out key
	
	return true;
}

bool qkdtools::QkdMgr::reset()
{
	if(!initDone)
	{
		return false;
	}

	if(myCurConfig.autoMode && myCurConfig.actAsAlice) //Alice sends cmd to Bob
	{
		char cmd=3; //reset
		int len=1;
		myNetCmd->sendData(&cmd, len);
	}

	//all threads working on the queues should be stopped

	//need to clear device from remaining data !
/*	if(!myDevice->clearDeviceDataStorage())
	{
		return false;
	}
*/
	//clear all four queues
	myThreadMgr->clearAllQueues();

	//start printout on new file (by adding date and time again)
	myPrintOut->closeFile();
	delete myPrintOut;
	myPrintOut=new qkdtools::printOutKey(addDateTimeToFilename(myCurConfig.printOutFilename), finalKeyQueue);

	return true;
}

bool qkdtools::QkdMgr::close()
{
	if(!initDone)
	{
		return false;
	}

	if(myCurConfig.autoMode && myCurConfig.actAsAlice) //Alice sends cmd to Bob
	{
		char cmd=4; //close
		int len=1;
		myNetCmd->sendData(&cmd, len);
	}

	myPrintOut->closeFile();

	myDevice->closeDev();

	if(myCurConfig.autoMode)
	{
		if(!myCurConfig.actAsAlice)
		{
			myCmdThreadCom.keepRunning=false;
			Sleep(10);
		}

		myNetCmd->closeSocket();
	}

	return true;
}
