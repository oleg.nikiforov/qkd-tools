/* NetworkMgr.cpp - manages network connections and sending/receiving data */

/*
Control software for the quantum key distribution experiment, developed and used in the Laser and Quantum Optics group (LQO) within the Technische Universität Darmstadt, Germany. 
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (c) 2012 by Pascal Notz
 */

#include "NetworkMgr.h"

qkdtools::NetworkMgr::NetworkMgr()
{
	initNetworkMgr(0);
}

qkdtools::NetworkMgr::NetworkMgr(int socketType)
{
	initNetworkMgr(socketType);
}

void qkdtools::NetworkMgr::initNetworkMgr(int socketType)
{

	this->socketType=socketType;

	wsockInitOk=false;
	wsockConnected=false;

	//start winsock
	if(WSAStartup(MAKEWORD(2,0),&wsa) == 0) //=0 -> OK
	{
		wsockInitOk = true;
	}

	svrSocket = NULL;
}

qkdtools::NetworkMgr::~NetworkMgr()
{
	closeSocket();
	WSACleanup();
}

//set parameters
void qkdtools::NetworkMgr::setNetworkParam(char* connectToIP, int connectToPort, int bindToPort)
{
	this->connectToIP=connectToIP;
	this->connectToPort=connectToPort;
	this->bindToPort=bindToPort;
}

//client type
bool qkdtools::NetworkMgr::connectSocket()
{
	if(!wsockInitOk)
		return false;

	//check if socket is of the right type
	if(socketType == 2) //serverType
	{
		return false;
	}
	else //define client type
	{
		socketType = 1;
	}

	//create socket
	wSocket=socket(AF_INET,SOCK_STREAM,0);

	if(wSocket==INVALID_SOCKET)
	{ 
		return false;
	}

	//reset address
	memset(&wAddress,0,sizeof(SOCKADDR_IN));

	wAddress.sin_family=AF_INET; //TCP protocol

	//set port for connection
	wAddress.sin_port=htons(connectToPort);

	//set address of server
	wAddress.sin_addr.s_addr=inet_addr(connectToIP);
	
	//connect socket
	if(connect(wSocket,(SOCKADDR*)&wAddress,sizeof(SOCKADDR)) == SOCKET_ERROR)
	{
		wsockConnected = false;
	}
	else
	{
		wsockConnected=true;
	}

	return wsockConnected;
}

//server type
bool qkdtools::NetworkMgr::bindSocket()
{
	if(!wsockInitOk)
		return false;

	//check if socket is of the right type
	if(socketType == 1) //clientType
	{
		return false;
	}
	else //define server type
	{
		socketType = 2;
	}

	//create socket
	svrSocket=socket(AF_INET,SOCK_STREAM,0);

	if(svrSocket==INVALID_SOCKET)
	{ 
		return false;
	}

	memset(&wAddress,0,sizeof(SOCKADDR_IN));

	//TCP protocol
	wAddress.sin_family=AF_INET;

	//set port to bind to
	wAddress.sin_port=htons(bindToPort);

	//allow any other IP address
	wAddress.sin_addr.s_addr=ADDR_ANY;

	//bind socket
	if(bind(svrSocket,(SOCKADDR*)&wAddress,sizeof(SOCKADDR_IN)) == SOCKET_ERROR)
	{
		return false;
	}
	else
	{
		//listen (waits for incoming connection request)
		if(listen(svrSocket,10) == SOCKET_ERROR)
		{
			return false;
		}
		else
		{
			//accept request, now we have a connection
			wSocket=accept(svrSocket,NULL,NULL);

			if(wSocket==INVALID_SOCKET) 
			{
				wsockConnected = false;
			}
			else
			{
				closesocket(svrSocket);
				wsockConnected = true;
			}

			return wsockConnected;
		}
	}

}

//close socket, for cleanup
void qkdtools::NetworkMgr::closeSocket()
{
	if(wSocket!=INVALID_SOCKET) 
	{
		closesocket(wSocket);
	}

	wsockConnected = false;
}

//send data on connected socket
bool qkdtools::NetworkMgr::sendData(char *data, int &len)
{
	if(!wsockInitOk || socketType==0 || !wsockConnected) //undefined socket
		return false;

	len = send(wSocket, data, len, 0);
	if(len == SOCKET_ERROR)
	{
		return false;
	}
	else
	{
		return true;
	}
}

//wait to receive data on connected socket
bool qkdtools::NetworkMgr::receiveData(char *data, int &len)
{
	if(!wsockInitOk || socketType==0 || !wsockConnected) //undefined socket
		return false;

	len = recv(wSocket, data, len, 0);
	if(len == SOCKET_ERROR)
	{
		return false;
	}
	else
	{
		return true;
	}
}

//receive data of defined length (call receiveData until we have received enough)
bool qkdtools::NetworkMgr::receiveDataOfLen(char *data, int len)
{
	int remainingBufLen;
	int totalBytesReceived=0;

	while(totalBytesReceived < len) //did not receive enough data
	{
		remainingBufLen = len - totalBytesReceived; //size of remaining memory

		//receive more data
		if(!receiveData(data + totalBytesReceived, remainingBufLen))
		{
			return false;
		}

		totalBytesReceived += remainingBufLen; //count number of bytes received with this call
	}

	return true;
}

//split data in packets of size 1 KB and send them
void qkdtools::NetworkMgr::sendDataInPackets(char *data, int len, char &retVal)
{
	int retValLen;
	int noOfPackets = (len / 1024); //number of packets with size 1KB)
				
	if(len%1024 != 0)
	{
		//add final packet
		noOfPackets++;
	}

	for(int pID=0;pID<noOfPackets;pID++) //transmit all packets
	{
		int pLength=1024;
					
		if(pID==noOfPackets-1) //last packet
		{
			pLength = len - pID*1024;
		}
					
		this->sendData(data + pID*1024, pLength);

		//wait for ok
		retValLen=1;
		this->receiveData(&retVal, retValLen);
		if(retVal != 0) //if not ok
		{
			//restart whole process
			continue;
		}
	}
}

//receive data that is split up into packets of 1 KB
void qkdtools::NetworkMgr::receiveDataInPackets(char *data, int len, char &retVal)
{
	int retValLen;
	int noOfPackets = (len / 1024); //number of packets with size 1KB)
				
	if(len%1024 != 0)
	{
		//add final packet
		noOfPackets++;
	}

	for(int pID=0;pID<noOfPackets;pID++) //receive all packets
	{
		int pLength=1024;

		if(pID==noOfPackets-1) //last packet
		{
			pLength = len - pID*1024;
		}
					
		this->receiveData(data + pID*1024, pLength);

		retVal = (pLength>0)?0:1;

		retValLen=1;
		this->sendData(&retVal, retValLen);

		if(retVal != 0) //if not ok
		{
			//restart whole process
			continue;
		}
	}

}

