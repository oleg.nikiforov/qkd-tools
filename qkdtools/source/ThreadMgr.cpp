/* ThreadMgr.cpp - manages threads and queues for post processing */

/*
Control software for the quantum key distribution experiment, developed and used in the Laser and Quantum Optics group (LQO) within the Technische Universität Darmstadt, Germany. 
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (c) 2012 by Pascal Notz
 */

#include "ThreadMgr.h"

qkdtools::ThreadMgr::ThreadMgr(bool actAsAlice)
{
	initThreadMgr(actAsAlice);
}

qkdtools::ThreadMgr::ThreadMgr()
{
	initThreadMgr(true);
}

void qkdtools::ThreadMgr::initThreadMgr(bool actAsAlice)
{
	//create network mgrs
	for(int i=0;i<3;i++)
	{
		netmgr[i]=new qkdtools::NetworkMgr();
	}

	//set params to empty values
	paKeyLength=-1;
	pchk_file=NULL;
	gen_file=NULL;
	hasNetworkParam=false;
	
	setActAsAlice(actAsAlice);

	//init queues
	validQueue=new qkdtools::KeyQueue();
	ecQueue=new qkdtools::KeyQueue();
	skQueue=NULL;
	finalQueue=NULL;
}

void qkdtools::ThreadMgr::setActAsAlice(bool actAsAlice)
{
	this->actAsAlice=actAsAlice;
	initThreadCom();
}

bool qkdtools::ThreadMgr::startNetworkConnection(int id)
{
	//open network connection
	if(actAsAlice) //this is alice, connecting to Bob
	{
		if(!netmgr[id]->connectSocket())
		{
			return false;
		}
	}
	else //this is bob, waiting for alice to connect
	{
		if(!netmgr[id]->bindSocket())
		{
			return false;
		}
	}

	return true;
}

void qkdtools::ThreadMgr::initThreadCom()
{
	for(int i=0;i<3;i++) //for all three threads
	{
		myThreadCom[i].netdevice = this->netmgr[i];
		myThreadCom[i].actAsAlice = this->actAsAlice; //Alice sends her packet valid information first, Bob responds
	}
}

void qkdtools::ThreadMgr::setKeyQueue(qkdtools::KeyQueue *queue)
{
	skQueue=queue;
}

void qkdtools::ThreadMgr::setFinalKeyQueue(qkdtools::KeyQueue *queue)
{
	finalQueue=queue;
}

bool qkdtools::ThreadMgr::readyToStart()
{
	return (paKeyLength>0 && hasNetworkParam && pchk_file!=NULL && gen_file!=NULL &&
			skQueue!=NULL && validQueue!=NULL && ecQueue!=NULL && finalQueue!=NULL);
}

void qkdtools::ThreadMgr::setNetworkData(char* connectToIP, int Port1, int Port2, int Port3)
{

	netmgr[0]->setNetworkParam(connectToIP, Port1, Port1);
	netmgr[1]->setNetworkParam(connectToIP, Port2, Port2);
	netmgr[2]->setNetworkParam(connectToIP, Port3, Port3);

	hasNetworkParam=true;
}

void qkdtools::ThreadMgr::setPAKeyLength(int paKeyLen)
{
	this->paKeyLength = paKeyLen;
}

void qkdtools::ThreadMgr::setLDPCMatFiles(char* pchk_filename, char* gen_filename)
{
	pchk_file=pchk_filename;
	gen_file=gen_filename;
}

bool qkdtools::ThreadMgr::startThread(int id)
{
	switch(id)
	{
	case 0:
		return startValidThread();
		break;
	case 1:
		return startECThread();
		break;
	case 2:
		return startPAThread();
		break;
	}
	
	return false;
}

bool qkdtools::ThreadMgr::stopThread(int id)
{
	myThreadCom[id].keepRunning=false;

	netmgr[id]->closeSocket();

	return true;
}

//delete invalid packets
bool qkdtools::ThreadMgr::startValidThread() //id=0
{
	//check if we have all parameters
	if(!readyToStart())
	{
		return false;
	}

	//start network connection for this thread
	if(!startNetworkConnection(0))
	{
		return false;
	}

	unsigned threadID;

	//set queues to work on
	myThreadCom[0].inQueue = this->skQueue;
	myThreadCom[0].outQueue = this->validQueue;
	myThreadCom[0].keepRunning=true;

	//start thread
	uintptr_t myValidThread = _beginthreadex(NULL, 0, deleteInvalidPackets, &myThreadCom[0], 0, &threadID);
	
	if(myValidThread == NULL)
	{
		return false;
	}

	//return true if thread was started
	return true;
}

//Error Correction
bool qkdtools::ThreadMgr::startECThread() //id=1
{
	//check if we have all parameters
	if(!readyToStart())
	{
		return false;
	}

	//start network connection for this thread
	if(!startNetworkConnection(1))
	{
		return false;
	}

	unsigned threadID;
 
	myThreadCom[1].inQueue = this->validQueue;
	myThreadCom[1].outQueue = this->ecQueue;

	// ### TESTING ###
//	myThreadCom[1].inQueue = this->skQueue;
//	myThreadCom[1].outQueue = this->finalQueue;

	myThreadCom[1].ldpcmgr = new qkdtools::ldpcMgr(pchk_file, gen_file);
	myThreadCom[1].keepRunning=true;

	//start thread
	uintptr_t myECThread = _beginthreadex(NULL, 0, doErrorCorrection, &myThreadCom[1], 0, &threadID);
	
	if(myECThread == NULL)
	{
		return false;
	}

	return true;
}

//Error Correction
bool qkdtools::ThreadMgr::startPAThread() //id=2
{
	//check if we have all parameters
	if(!readyToStart())
	{
		return false;
	}

	//start network connection for this thread
	if(!startNetworkConnection(2))
	{
		return false;
	}

	unsigned threadID;


	myThreadCom[2].inQueue = this->ecQueue;
	myThreadCom[2].outQueue = this->finalQueue;

// ### TESTING ###
//myThreadCom[2].inQueue = this->skQueue;

	myThreadCom[2].keepRunning=true;
	myThreadCom[2].paKeyLength=paKeyLength;

	//start thread
	uintptr_t myPAThread = _beginthreadex(NULL, 0, doPrivacyAmplification, &myThreadCom[2], 0, &threadID);
	
	if(myPAThread == NULL)
	{
		return false;
	}

	return true;
}

void qkdtools::ThreadMgr::clearAllQueues()
{
	//check if the queues exist, if so clear them

	clearInternalQueues();

	if(skQueue != NULL)
	{
		skQueue->clear();
	}
	if(finalQueue != NULL)
	{
		finalQueue->clear();
	}
}

void qkdtools::ThreadMgr::clearInternalQueues()
{
	//check if the queues exist, if so clear them

	if(validQueue != NULL)
	{
		validQueue->clear();
	}
	if(ecQueue != NULL)
	{
		ecQueue->clear();
	}
}

