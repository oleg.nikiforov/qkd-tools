/* printOutKey.cpp - print out the key packets from a given queue, using a separate thread */

/*
Control software for the quantum key distribution experiment, developed and used in the Laser and Quantum Optics group (LQO) within the Technische Universität Darmstadt, Germany. 
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (c) 2012 by Pascal Notz
 */

#include "printOutKey.h"

qkdtools::printOutKey::printOutKey(char* filename, qkdtools::KeyQueue *queue)
{
	
	this->queue=queue;

	//open file for printout
	myThreadCom.fileoutput.open(filename);
	myThreadCom.keepRunning=false;

}

bool qkdtools::printOutKey::startPrintOut()
{
	if(myThreadCom.keepRunning) //thread still running
	{
		return false;
	}

	unsigned threadID;

	//set thread params
	myThreadCom.keepRunning=true;
	myThreadCom.queue = this->queue;

	//start thread
	uintptr_t myPrintOutThread = _beginthreadex(NULL, 0, printOutKeyQueue, &myThreadCom, 0, &threadID);

	if(myPrintOutThread == NULL)
	{
		return false;
	}

	return true;
}

void qkdtools::printOutKey::stopPrintOut()
{
	if(!myThreadCom.keepRunning) //if not still running
	{
		return;
	}

	myThreadCom.keepRunning=false;

}

void qkdtools::printOutKey::closeFile()
{
	//close file
	myThreadCom.fileoutput.close();
}

