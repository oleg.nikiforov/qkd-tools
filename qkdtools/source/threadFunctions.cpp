/* threadFunctions.cpp - provides functions for separate threads */

/*
Control software for the quantum key distribution experiment, developed and used in the Laser and Quantum Optics group (LQO) within the Technische Universität Darmstadt, Germany. 
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (c) 2012 by Pascal Notz
 *
 *
 * These functions are to be used only by the threads, they must not be called
 * in any other ways.
 *
 */

#include "DevMgr.h"
#include "printOutKey.h"
#include "ThreadMgr.h"
#include "PrivAmp.h"
#include <string>
#include <fstream>
#include <iomanip>

//takes key packets from a key and writes them to a file
unsigned __stdcall printOutKeyQueue(void * param)
{
	qkdtools::threadComPrintOut* thC = (qkdtools::threadComPrintOut*)param;

	qkdtools::KeyQueuePacket* packet = NULL;

	//main loop
	while(thC->keepRunning)
	{
		//get packet
		packet = thC->queue->popPacket();

		while(packet==NULL)
		{
			if(!thC->keepRunning)
			{
				_endthreadex(0);
				return 0;
			}

			//if queue is empty, wait 10ms and try again
			Sleep(10);
			packet = thC->queue->popPacket();
		}

		//we have a packet

		if(packet->isValid())
		{
			bool *data = packet->getData();
			std::string out;

			//if a QBER is set (default is -1), print out to file
			if(packet->getQBER() != -1)
			{
				thC->fileoutput << std::setprecision(6) << packet->getQBER() << "#";
			}

			//convert packet data to string
			for(int i=0; i<packet->length(); i++)
			{
				out += data[i]?"1":"0";
			}

			//write packet data to string
			thC->fileoutput << out;
		}
		else
		{
			thC->fileoutput << "ERROR in packet";
		}

		thC->fileoutput << std::endl;

		//delete packet and data
		delete [] packet->getData();
		delete packet;

	}

	_endthreadex(0);

	return 0;
}

//thread for bob to receive commands by Alice
unsigned __stdcall receiveCmds(void * param)
{
	qkdtools::threadComBobCmd* thC = (qkdtools::threadComBobCmd*)param;

	char cmdBuf[10];
	int cmdLen=10;

	//main loop
	while(thC->keepRunning)
	{
		cmdLen=0;
		//loop until we receive a command
		while(cmdLen==0 && thC->keepRunning)
		{
			Sleep(10);
			cmdLen=10; //buffer length
			thC->netdevice->receiveData(cmdBuf, cmdLen);
		}

		if(cmdBuf[0]==1) //start
		{
			std::cout << "Received Cmd: starting threads.." << std::endl;
			thC->myQkdMgr->start();
		}
		else if(cmdBuf[0]==2) //stop
		{
			std::cout << "Received Cmd: stopping threads.." << std::endl;
			thC->myQkdMgr->stop();
		}
		else if(cmdBuf[0]==3) //reset
		{
			std::cout << "Received Cmd: resetting queues.." << std::endl;
			thC->myQkdMgr->reset();
		}
		else if(cmdBuf[0]==4) //close
		{
			std::cout << "Received Cmd: close.." << std::endl;
			thC->myQkdMgr->close();
		}

	}

	_endthreadex(0);

	return 0;
}

//while keepRunning is set createSiftedKey downloads data from FPGA
//and splits it into blocks of BLOCKLENGTH bits,
//putting them into the KeyQueue
//BLOCKLENGTH is defined in qkdtools.h
unsigned __stdcall createSiftedKey(void * param)
{
	qkdtools::threadComDevMgr* thC = (qkdtools::threadComDevMgr*)param;

	//create buffer for download
	unsigned char buffer[65536];
	int bufferLength;

	//create packet data buffer
	bool *dataBuf = new bool[BLOCKLENGTH];
	int dataLen=0;

	//main loop
	while(thC->keepRunning)
	{
		//get new data from FPGA
		thC->device->getData(buffer, bufferLength);

		if(bufferLength > 0) //we have data
		{		//put data in BLOCKLENGTH bit packets
			
			int i=0;
			int startBit=dataLen;
			
			while((i+BLOCKLENGTH-startBit) < bufferLength) //while we have a full BLOCKLENGTH bit block
			{
				int j=0;
				//add bits until block separation symbol or block should be full (=BLOCKLENGTH)
				while( (startBit + j) < BLOCKLENGTH && !(buffer[i] & 0x2) )
				{
					if(buffer[i]&1) //valid key bit
					{
						dataBuf[startBit+j]=( (buffer[i]&128) | (buffer[i]&32) )==1?1:0; //extract KEY: ch2 / ch4 => 1, else => 0
						j++;
					}

					i++;
				}

				//create packet
				qkdtools::KeyQueuePacket *skPacket;

				if( !(buffer[i] & 0x2) || (startBit+j)<BLOCKLENGTH ) //no block end or wrong size, we have an invalid block
				{
					skPacket = new qkdtools::KeyQueuePacket(false); //invalid block packet

					while(i<bufferLength)
					{
						if((buffer[i] & 0x2)) //block sep signal found
						{
							break;
						}

						i++;
					}
				}
				else //block full -> create packet
				{
					skPacket = new qkdtools::KeyQueuePacket(true); //valid block packet
				}

				skPacket->setData(dataBuf);

				//put packet in queue
				thC->queue->pushPacket(skPacket);

				//new packet data buffer
				dataBuf = new bool[BLOCKLENGTH];
				startBit=0;

				if(i<bufferLength)
				{
					if(buffer[i] & 0x2) //block separation symbol
					{
						//goto next bit
						i++;
					}
				}
			} //while

			//save last bits that were not used
			int j=0;
			while(i < bufferLength)
			{
				if(buffer[i]&1) //valid key bit
				{
					dataBuf[startBit+j]=( (buffer[i]&128) | (buffer[i]&32) )==1?1:0; //extract KEY: ch2 / ch4 => 1, else => 0
					j++;
				}

				i++;
			}

			dataLen=startBit+j; //move starting point

		} //if we have data
	
		//Sleep(10);
	}

	_endthreadex(0);

	return 0;
}

//through communication between Alice and Bob, blocks that are invalid on either side are deleted
//valid blocks are put into the validKeyQueue for further processing
unsigned __stdcall deleteInvalidPackets(void * param)
{
	qkdtools::threadCom* thC = (qkdtools::threadCom*)param;

	qkdtools::KeyQueuePacket* packet = NULL;

	//main loop
	while(thC->keepRunning)
	{
		packet = thC->inQueue->popPacket();

		while(packet==NULL)
		{
			if(!thC->keepRunning)
			{
				_endthreadex(0);
				return 0;
			}

			//if queue is empty, wait 10ms and try again
			Sleep(10);
			packet = thC->inQueue->popPacket();

//##################################
//DEBUG
/*			if(packet==NULL)
			{
				std::cout << "siftedKey Queue seems empty! delInvPacket has nothing to do.. " << std::endl;
				Sleep(3000);
			}
*/
		}

		//now we have a packet

		char valid;

		if(thC->actAsAlice) //Alice: send data first
		{
			int len=1;
			valid = packet->isValid()?1:0;
			if(!thC->netdevice->sendData(&valid, len))
			{
				//network connection is down -> exit thread
				break;
			}
			if(!thC->netdevice->receiveData(&valid, len))
			{
				//network connection is down -> exit thread
				break;
			}

		}
		else //Bob: wait for data from Alice and respond
		{
			int len=1;
			if(!thC->netdevice->receiveData(&valid, len))
			{
				//network connection is down -> exit thread
				break;
			}

			if(valid == 1 && packet->isValid() ) //our packet and hers are boths valid
			{
				valid=1;
			}
			else //at least one packet is not valid
			{
				valid=0;
			}
			len=1;

			if(!thC->netdevice->sendData(&valid, len))
			{
				//network connection is down -> exit thread
				break;
			}
		}

		if(valid == 1) //valid packet, copy into validQueue
		{
			thC->outQueue->pushPacket(packet);
		}
		else //delete packet
		{
			delete [] (packet->getData());
			delete packet;
		}
		
	}

	_endthreadex(0);

	return 0;

}

//QBER estimation and error correction with LDPC:
//both Alice and Bob collect 11 packets of BLOCKLENGTH bits,
//then Alice chooses and deletes BLOCKLENGTH bits for QBER estimation
//Alice computes the LDPC parity, Bob corrects his block of 10*BLOCKLENGTH bits
//the resulting key is put in the ecQueue
unsigned __stdcall doErrorCorrection(void * param)
{
	qkdtools::threadCom* thC = (qkdtools::threadCom*)param;

	qkdtools::KeyQueuePacket* packet = NULL;

	//std::ofstream fileoutput("d:\\ec_ldpckey.txt");

	//for BLOCKLENGTH 100 and LDPC size 1000 we need 11 packets, after qber 10 packets
	char* srcQBER= new char[BLOCKLENGTH * 11];
	char* src= new char[BLOCKLENGTH * 10];
	char* outdata;

	//main loop
	while(thC->keepRunning)
	{
		for(int curPacketNumber=0; curPacketNumber < 11; curPacketNumber++)
		{
			packet = thC->inQueue->popPacket();

			while(packet==NULL)
			{
				if(!thC->keepRunning)
				{
					_endthreadex(0);
					return 0;
				}

				//if queue is empty, wait 10ms and try again
				Sleep(10);
				packet = thC->inQueue->popPacket();

//##################################
//DEBUG
/*				if(packet==NULL)
				{
					std::cout << "validKey Queue seems empty! EC thread has nothing to do.. " << std::endl;
					Sleep(3000);
				}
*/
			}

			bool *bdata=packet->getData();

			//set packet in datarange
			for(int i=0;i<BLOCKLENGTH;i++)
			{
				if(bdata[i])
				{
					srcQBER[BLOCKLENGTH * curPacketNumber + i]=1;
				}
				else
				{
					srcQBER[BLOCKLENGTH * curPacketNumber + i]=0;
				}
			}
			
			//delete packet
			delete [] packet->getData();
			delete packet;
		}

		float curQBER;

		if(thC->actAsAlice) //Alice: send QBER data, wait for response
		{
			//choose bits for QBER
			int *bitIDVal = new int[2 * BLOCKLENGTH]; //0-99 = bit ID, 100-199 = value
			for(int i=0;i<BLOCKLENGTH;i++)
			{
				int bitID=rand() % (BLOCKLENGTH * 11); //0-1099

				while(srcQBER[bitID]==-1) //new random number if we used that bit already
				{
					bitID=rand() % BLOCKLENGTH * 11;
				}

				//store the randomly choosen bit and delete it from the key src
				bitIDVal[i]=bitID;
				bitIDVal[BLOCKLENGTH + i] = srcQBER[bitID];
				srcQBER[bitID]=-1;
			}

			//transmit bits and values
			int len=2 * BLOCKLENGTH * sizeof(int);

			if(!thC->netdevice->sendData((char*)bitIDVal, len))
			{
				//network connection is down -> exit thread
				break;
			}
			
			//wait for Bobs calculation of QBER
			char *tmpPtr=(char*) &curQBER;
			len=sizeof(float);

			if(!thC->netdevice->receiveData(tmpPtr, len))
			{
				//network connection is down -> exit thread
				break;
			}

			//fileoutput << "QBER: " << curQBER << std::endl;

			delete [] bitIDVal;

		}
		else //Bob: wait for Alice's QBER data, calc QBER and transmit it to Alice
		{
			int *bitIDVal = new int[2 * BLOCKLENGTH];
			char *tmpPtr=(char*) bitIDVal;
			int len=2 * BLOCKLENGTH * sizeof(int);

			if(!thC->netdevice->receiveDataOfLen(tmpPtr, len))
			{
				//network connection is down -> exit thread
				break;
			}

			//calc QBER
			int bitErrs=0;
			for(int i=0;i<BLOCKLENGTH;i++)
			{
				int bitID=bitIDVal[i];
				int val=bitIDVal[BLOCKLENGTH + i];

				if(srcQBER[bitID] != val)
				{
					//bit error
					bitErrs++;
				}

				srcQBER[bitID]=-1; //delete bit from key src
			}

			curQBER=(float)bitErrs / (float)BLOCKLENGTH;

			//set QBER for LDPC decoding:
			thC->ldpcmgr->setErrorProbabilityForDec(curQBER);

			len = sizeof(float);

			if(!thC->netdevice->sendData((char*)&curQBER, len))
			{
				//network connection is down -> exit thread
				break;
			}
			
			//fileoutput << "QBER: " << curQBER << std::endl;

			delete [] bitIDVal;
		}


		//copy unused bits to new src array
		int srcID=0;
		for(int i=0;i<BLOCKLENGTH * 11;i++)
		{
			if(srcQBER[i] != -1)
			{
				src[srcID]=srcQBER[i];
				srcID++;
			}
		}

		//now we have a full datarange of 10*BLOCKLENGTH bits
		bool decValid;

		if(thC->actAsAlice) //encode data and transmit
		{
			thC->ldpcmgr->encode(src);
			char* par = thC->ldpcmgr->getParityData();

			int len=thC->ldpcmgr->getCodewordLength();

			if(!thC->netdevice->sendData(par, len))
			{
				//network connection is down -> exit thread
				break;
			}

			outdata=src;
			
			len=1;
			char isValid;

			if(!thC->netdevice->receiveData(&isValid, len))
			{
				//network connection is down -> exit thread
				break;
			}

			decValid=(isValid==1);

			free(par); //allocated by ldpcmgr, C-based calloc(.)
		}
		else //wait for parity data from Alice
		{
			int len = thC->ldpcmgr->getCodewordLength();
			char* par = new char[len * sizeof(char)];

			if(!thC->netdevice->receiveDataOfLen(par, len))
			{
				//network connection is down -> exit thread
				break;
			}

			decValid = thC->ldpcmgr->decodeParityAndData(par, src);

			outdata = thC->ldpcmgr->getDecodedData();

			len=1;
			char isValid=decValid?1:0;
			
			if(!thC->netdevice->sendData(&isValid, len))
			{
				//network connection is down -> exit thread
				break;
			}

			delete [] par;
		}

		if(decValid)
		{
			//put valid result in packet in ECQueue
			bool *dataBuf = new bool[BLOCKLENGTH * 10]; //1000
			for(int i=0; i<1000; i++)
			{
				dataBuf[i] = outdata[i]; //==1?1:0;
			}

			if(!thC->actAsAlice) //for Bob only
			{
				free(outdata); //allocated by ldpcmgr, C-based calloc(.)
			}

			packet = new qkdtools::KeyQueuePacket(true);
			packet->setData(dataBuf);
			packet->setLength(BLOCKLENGTH * 10);
			packet->setQBER(curQBER);
			
			thC->outQueue->pushPacket(packet);

/*
			std::string out;

			for(int i=0; i<10*BLOCKLENGTH; i++)
			{
				out += outdata[i]==1?"1":"0";
			}

			fileoutput << out << std::endl;
*/
		}
		else
		{
			//fileoutput << "got invalid packet" << std::endl;
		}

	}

	//fileoutput.close();

	delete [] srcQBER;
	delete [] src;

	_endthreadex(0);

	return 0;

}

//Alice generates a Toeplitz matrix and transmits it to Bob
//both calculate the privacy amplified key
unsigned __stdcall doPrivacyAmplification(void * param)
{
	qkdtools::threadCom* thC = (qkdtools::threadCom*)param;

	qkdtools::KeyQueuePacket* packet = NULL;

	//std::ofstream fileoutput("d:\\pa_key.txt");

	//PA KeyLength
	qkdtools::PrivAmp pa(thC->paKeyLength);

	//mainloop
	while(thC->keepRunning)
	{
		packet = thC->inQueue->popPacket();

		while(packet==NULL)
		{
			if(!thC->keepRunning)
			{
				_endthreadex(0);
				return 0;
			}

			//if queue is empty, wait 10ms and try again
			Sleep(10);
			packet = thC->inQueue->popPacket();

//##################################
//DEBUG
/*			if(packet==NULL)
			{
				std::cout << "EC Key Queue seems empty! PA thread has nothing to do.. " << std::endl;
				Sleep(3000);
			}
*/
		}

		//now we have a packet

		if(thC->actAsAlice) //Alice: generate Toeplitz-Matrix and send it to Bob
		{
			pa.generateToeplitzMat();
			int len=pa.getToeplitzMatLen();
			
			if(!thC->netdevice->sendData(pa.getToeplitzMat(), len))
			{
				//network connection is down -> exit thread
				break;
			}
			
			//receive ok signal to keep both threads kind of synchronized
			char retVal;
			int retValLen=1;
			if(!thC->netdevice->receiveData(&retVal, retValLen))
			{
				//network connection is down -> exit thread
				break;
			}
			
		}
		else //Bob: receive Toeplitz Matrix from Alice
		{
			int len=pa.getToeplitzMatLen(); 
			
			if(!thC->netdevice->receiveDataOfLen(pa.getToeplitzMat(), len))
			{
				//network connection is down -> exit thread
				break;
			}

			//send ok signal to keep both threads kind of synchronized
			char retVal=0;
			int retValLen=1;
			if(!thC->netdevice->sendData(&retVal, retValLen))
			{
				//network connection is down -> exit thread
				break;
			}
		}

		//calculate privacy amplified key
		pa.calcPAKey(packet->getData());
		float curQBER = packet->getQBER();

		//delete old packet
		delete [] (packet->getData());
		delete packet;

		//create new packet / data
		bool *dataBuf = new bool[thC->paKeyLength];
		pa.copyPAKey(dataBuf);
/*
		std::string out;
		for(int i=0; i<900; i++)
		{
			out += dataBuf[i]==1?"1":"0";
		}
		fileoutput << out << std::endl;
*/
		//create packet
		packet = new qkdtools::KeyQueuePacket(true);
		packet->setData(dataBuf);
		packet->setLength(thC->paKeyLength);
		packet->setQBER(curQBER);

		//put packet into queue
		thC->outQueue->pushPacket(packet);
		
	}

	//fileoutput.close();

	_endthreadex(0);

	return 0;

}

