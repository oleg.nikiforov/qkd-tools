/* KeyQueue.cpp - key queue and key queue packet to store key parts, FIFO queue */
/*
Control software for the quantum key distribution experiment, developed and used in the Laser and Quantum Optics group (LQO) within the Technische Universität Darmstadt, Germany. 
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (c) 2012 by Pascal Notz
 */

#include "KeyQueue.h"

//the packet
qkdtools::KeyQueuePacket::KeyQueuePacket()
{
	//if no valid bit is set, assume invalid packet
	initQueuePacket(false);
}

qkdtools::KeyQueuePacket::KeyQueuePacket(bool valid)
{
	initQueuePacket(valid);
}

void qkdtools::KeyQueuePacket::initQueuePacket(bool valid)
{
	this->valid = valid;

	//init packet parameters (not all need to be set)
	this->next = NULL;
	this->data = NULL;
	this->len = 0;
	this->qber = -1.0f;
}

bool qkdtools::KeyQueuePacket::isValid()
{
	return valid;
}

qkdtools::KeyQueuePacket* qkdtools::KeyQueuePacket::getNext()
{
	return next;
}

void qkdtools::KeyQueuePacket::setNext(KeyQueuePacket* n)
{
	this->next = n;
}

bool* qkdtools::KeyQueuePacket::getData()
{
	return data;
}

void qkdtools::KeyQueuePacket::setData(bool *d)
{
	this->data = d;
}

void qkdtools::KeyQueuePacket::setLength(int length)
{
	len=length;
}

int qkdtools::KeyQueuePacket::length()
{
	return len;
}

void qkdtools::KeyQueuePacket::setQBER(float qber)
{
	this->qber = qber;
}

float qkdtools::KeyQueuePacket::getQBER()
{
	return qber;
}


//the queue
qkdtools::KeyQueue::KeyQueue()
{
	this->first = NULL;
	this->last = NULL;
}
		
//push packet -> except for first packet, only *last will be modified
void qkdtools::KeyQueue::pushPacket(qkdtools::KeyQueuePacket* p)
{
	if(last == NULL) //empty list
	{
		first = p;
		last = p;
	}
	else
	{
		//append packet to list
		last->setNext(p);
		last = p;
		last->setNext(NULL);
	}
}

//pop packet -> takes first packet out of queue, only *first will be modified
qkdtools::KeyQueuePacket* qkdtools::KeyQueue::popPacket()
{ //does not return last packet -> so we don't need mutex
	qkdtools::KeyQueuePacket* p = NULL;
	
	if(first != NULL) //make sure list is not empty
	{
		if(first->getNext() != NULL) //only return packet if it is not the last one
		{
			p = first;
			first = first->getNext();
		}

/*		if(first == NULL) //it was the last packet
		{
			last = NULL;
		}
		*/

	}

	return p;
}

//clear the queue: delete all packets
void qkdtools::KeyQueue::clear()
{
	qkdtools::KeyQueuePacket* p = popPacket();

	while(p != NULL)
	{
		if(p->getData() != NULL)
		{
			//delete packet data
			delete [] (p->getData());
		}
		
		//now delete packet
		delete p;

		//get next packet
		p = popPacket();
	}

	//need to delete first packet, that won't be popped
	if(first != NULL)
	{
		if(first->getData() != NULL)
		{
			//delete packet data
			delete [] (first->getData());
		}
		
		//now delete packet
		delete first;

		first = NULL;
		last = NULL;
	}
}

