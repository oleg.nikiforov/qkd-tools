/* qkdtools.cpp - provide functions for FPGA delay optimization */

/*
Control software for the quantum key distribution experiment, developed and used in the Laser and Quantum Optics group (LQO) within the Technische Universität Darmstadt, Germany. 
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (c) 2012 by Pascal Notz
 */

#include "qkdtools.h"
using namespace std;
using namespace fpgaIO;



BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
// Defines the entry point for the DLL application.					 
{
    return TRUE;
}


///////////////////////////////////////////////////
// Run once with specific delay
///////////////////////////////////////////////////
_declspec(dllexport) bool qkdtools::runDataCollectOnce(int *delays) //expects array delays[5] for channel 0 - 3 + global delay
{

	//check if local delays are valid => 0 to 3, smaller than 128
	for(int i=0;i<4;i++)
	{
		if(delays[i]>127)
		{
			return false;
		}
	}

	if(delays[4]>4095)
		return false;

	//open FPGA connection
	HANDLE fpgaHandle=FPGA_Open();

	//buffer to receive data
	unsigned char buffer[65536];
	int bufferLength;

	if((int)fpgaHandle==-1)
		return false;

	unsigned char *myCmd=(unsigned char*)malloc(6 * sizeof(unsigned char));
		
	myCmd[0]=2; //stop
	FPGA_Interface(fpgaHandle, myCmd, buffer, bufferLength);

	//set delay: detector signal will be one cycle early
	for(int j=0;j<4;j++)
	{
		myCmd[j]=128 + delays[j]; //128: [7] = 1 / delay bestimmung!
	}
	myCmd[4] = 128 + (delays[4]%128); //lower bits
	myCmd[5] = 128 + (int)(delays[4] / 128); //upper bits

	//set delays
	FPGA_Interface(fpgaHandle, myCmd, buffer, bufferLength);
	
	myCmd[0]=4; //start=4, justage=8
	for(int j=1;j<6;j++)
	{
		myCmd[j]=0;
	}

	//start it
	FPGA_Interface(fpgaHandle, myCmd, buffer, bufferLength);
	Sleep(100); //wait so we can collect data

	myCmd[0]=0; //just run

	for(int i=0;i<1;i++) //collect data once
	{	
		FPGA_Interface(fpgaHandle, myCmd, buffer, bufferLength);
	}

	myCmd[0]=2; //stop it
	FPGA_Interface(fpgaHandle, myCmd, buffer, bufferLength);

	FPGA_Close(fpgaHandle);

	return true;
}

int countSuccessfulEvents(unsigned char *buffer, int bufferLength)
{
	int retVal=0;

	for(int i=0;i<bufferLength;i++)
	{
		if(buffer[i] & 0x1) //ok=1
		{
			retVal++;
		}
	}

	return retVal;
}

///////////////////////////////////////////////////
// Determine Optimal Delays for Channels 1-4
///////////////////////////////////////////////////
_declspec(dllexport) int qkdtools::optimizeGlobalDelay(int *localDelays, int DelayGuess) //returns delay
{

	//connect to FPGA
	HANDLE fpgaHandle=FPGA_Open();

	if((int)fpgaHandle==-1)
		return -1;

	//run optimizeDelays
	int retVal = optimizeGlobalDelay(fpgaHandle, localDelays, DelayGuess);

	FPGA_Close(fpgaHandle);

	return retVal;
}

//channel has to be [0,3]
_declspec(dllexport) int qkdtools::optimizeGlobalDelay(HANDLE XyloDeviceHandle, int *localDelays, int DelayGuess) //returns delay
{
	const int Steps = 5; // search range [DelayGuess-Steps;DelayGuess+Steps] for delay

#ifdef PRINTOUT_DEBUG_FILE
	char * filename=PRINTOUT_DEBUG_FILENAME;
	remove(filename);
	fstream myOutputfile(filename,fstream::app|fstream::out|fstream::binary);
#endif

	if((int)XyloDeviceHandle==-1 || localDelays==NULL || (DelayGuess+5)>4095 )
		return -1;

	//check if local delays are valid => 0 to 3, smaller than 128
	for(int i=0;i<4;i++)
	{
		if(localDelays[i]>127)
		{
			return -1;
		}
	}


	float successfulEvents[2 * Steps + 1]; //divided by all events, for [DelayGuess-Steps;DelayGuess+Steps]

	if( (DelayGuess - Steps) < 0 )
	{
		DelayGuess = Steps;
	}

	//create Buffer for FPGA data
	unsigned char buffer[65536];
	int bufferLength;

	//create command to pass to FPGA
	unsigned char *myCmd=(unsigned char*)malloc(6 * sizeof(unsigned char));
	
	for(int i=0;i<6;i++)
	{
		myCmd[i]=0;
	}

	myCmd[0]=4; //start=4, justage=8
	FPGA_Interface(XyloDeviceHandle, myCmd, buffer, bufferLength); //start device
	
	Sleep(10); //let FPGA collect some data

	//try all delays of range and check for coincidences
	for(int testDelay=DelayGuess-Steps;testDelay<=DelayGuess+Steps;testDelay++)
	{
		//set delay: detector signal will be one cycle early
		for(int j=0;j<4;j++)
		{
			myCmd[j]=128 + localDelays[j]; //128: [7] = 1 / delay bestimmung!
		}
		myCmd[4] = 128 + (testDelay%128); //lower bits
		myCmd[5] = 128 + (int)(testDelay / 128); //upper bits

		FPGA_Interface(XyloDeviceHandle, myCmd, buffer, bufferLength); //set delays

		//reset command buffer
		for(int j=1;j<6;j++)
		{
			myCmd[j]=0;
		}
		myCmd[0]=4; //start again
		FPGA_Interface(XyloDeviceHandle, myCmd, buffer, bufferLength);

		Sleep(10);
		myCmd[0]=0; //do nothing, collect data
		FPGA_Interface(XyloDeviceHandle, myCmd, buffer, bufferLength);
		Sleep(10);
		FPGA_Interface(XyloDeviceHandle, myCmd, buffer, bufferLength); //hope to get fresh data now

		if(bufferLength>0)
		{	//hier: vergleich relativer matches, alternative: absolute anzahl an treffern maximieren! (da doppelcounts wg. "old[]")
			successfulEvents[testDelay - (DelayGuess-Steps)]=(float)(countSuccessfulEvents(buffer, bufferLength)) / (float)bufferLength;
#ifdef PRINTOUT_DEBUG_FILE
			myOutputfile << successfulEvents[testDelay - (DelayGuess-Steps)] << std::endl;
#endif
		}
	}

	//find maximum
	float curMax=-1;
	int curMaxID=-1;
	for(int i=0;i<(2 * Steps + 1);i++)
	{
		if(successfulEvents[i]>curMax)
		{
			curMax=successfulEvents[i];
			curMaxID=i;
		}
	}

	myCmd[0]=2; //stop
	FPGA_Interface(XyloDeviceHandle, myCmd, buffer, bufferLength);
#ifdef PRINTOUT_DEBUG_FILE
	myOutputfile.close();
#endif
	return (DelayGuess-Steps + curMaxID);
}
